package web.utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

/**
 * Created by TamTran on 12/24/2016.
 */
public class Browser {
    private static String browserType;
    private static String browserPath;
    private static ChromeOptions options;
    /**
     * Return a browser instance based on parameters above
     */
    public static WebDriver getInstance(String inBrowserType) {

        browserType = inBrowserType;
        browserPath = System.getProperty("user.dir") + "\\resources\\browser-drivers\\";
        WebDriver driver = buildDriver();
        driver.manage().window().maximize();
        return driver;
    }

    /**
     * Build a "local" browser instance
     */
    private static WebDriver buildDriver() {
        WebDriver driver = null;

        switch (browserType.toLowerCase()) {
            case "firefox":
                System.setProperty("webdriver.gecko.driver", browserPath + "\\geckodriver.exe");
                driver = new FirefoxDriver();
                break;

            case "chrome":
                System.setProperty("webdriver.chrome.driver", browserPath + "\\chromedriver.exe");
                //options=new ChromeOptions();
                //options.addArguments("disable-infobars");
                //driver = new ChromeDriver(options);
                driver = new ChromeDriver();
                break;

            case "ie":
                System.setProperty("webdriver.ie.driver", browserPath + "\\IEDriverServer.exe");
                driver = new InternetExplorerDriver();
                break;

        }

        return driver;
    }

}

