package com.tietkiemnhom.test;

import com.tietkiemnhom.pages.TietKiemNhom;
import org.openqa.selenium.WebDriver;
import org.testng.TestNG;
import org.testng.annotations.*;
import web.utilities.Browser;
import web.utilities.Common;
import web.utilities.ExcelUtils;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Created by TamTran on 6/3/2017.
 */
public class TietKiemNhomTest {
    private WebDriver driver;
    private TietKiemNhom tkn;
    public static final String URL = "https://app.tietkiemnhom.com/";

    @Parameters("browser")
    @BeforeClass
    private void beforeTC(String browser) {
        System.out.println("TietKiemNhomTest.beforeTC");
        driver = Browser.getInstance(browser);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(URL);
        tkn = new TietKiemNhom(driver);
        //tkn.handlePopUp();
        ExcelUtils.setWorkbook(System.getProperty("user.dir") + File.separator + "/resources" + File.separator + "test-cases" + File.separator + "tietkiemnhom.com.xls", 0);
    }

    @AfterClass
    public void endTC() {
        System.out.println("TietKiemNhomTest.endTC");
        driver.quit();
        ExcelUtils.saveAs(System.getProperty("user.dir") + File.separator + "/resources" + File.separator + "test-results" + File.separator + "report-tietkiemnhom.com.xls");
    }

    @Test(description = "TC1: Kiểm tra [Số điện thoại] và [Mật khẩu] hợp lệ")
    public void verifyLoginPassed() {
        tkn = tkn.resetData()
                .typePhone("01264680025")
                .typePassword("123TamTV")
                .clickTiepTuc();
        tkn.clickThoat();
        Common.sleep(2000);
    }

    @Test(description = "TC2: Kiểm tra [Số điện thoại] hợp lệ, [Mật khẩu] không tồn tại")
    public void verifyPasswordValidation() {
        tkn = tkn.resetData()
                .typePhone("01264680025")
                .typePassword("abc@@@123")
                .clickTiepTuc();
        Common.sleep(2000);
    }

    @Test(description = "TC3: Kiểm tra [Số điện thoại] hợp lệ, không nhập [Mật khẩu]")
    public void verifyPasswordDoNotEnter() {
        tkn = tkn.resetData()
                .typePhone("01264680025")
                .typePassword("")
                .clickTiepTuc();
        Common.sleep(2000);
    }

    @Test(description = "TC4: Kiểm tra [Số điện thoại] không hợp lệ, [Mật khẩu] hợp lệ")
    public void verifyPhoneValidation() {
        tkn = tkn.resetData()
                .typePhone("abcdefgh")
                .typePassword("123TamTV")
                .clickTiepTuc();
        Common.sleep(2000);
    }

    @Test(description = "TC5: Kiểm tra [Số điện thoại] không hợp lệ, [Mật khẩu] không hợp lệ")
    public void verifyPhonePasswordValidation() {
        tkn = tkn.resetData()
                .typePhone("abcdefgh")
                .typePassword("abc@@@123")
                .clickTiepTuc();
        Common.sleep(2000);
    }

    @Test(description = "TC6: Kiểm tra [Số điện thoại] không hợp lệ, không nhập [Mật khẩu]")
    public void verifyPhoneValidation_PasswordDoNotEnter() {
        tkn = tkn.resetData()
                .typePhone("abcdefgh")
                .typePassword("")
                .clickTiepTuc();
        Common.sleep(2000);
    }

    @Test(description = "TC7: Kiểm tra không nhập [Số điện thoại], [Mật khẩu] hợp lệ")
    public void verifyPhoneDoNotEnter() {
        tkn = tkn.resetData()
                .typePhone("")
                .typePassword("123TamTV")
                .clickTiepTuc();
        Common.sleep(2000);
    }

    @Test(description = "TC8: Kiểm tra không nhập [Số điện thoại], [Mật khẩu] không hợp lệ")
    public void verifyPhoneDoNotEnter_PasswordValidation() {
        tkn = tkn.resetData()
                .typePhone("")
                .typePassword("abc@@@123")
                .clickTiepTuc();
        Common.sleep(2000);
    }

    @Test(description = "TC9: Kiểm tra không nhập [Số điện thoại] và [Mật khẩu]")
    public void verifyPhonePasswordDoNotEnter() {
        tkn = tkn.resetData()
                .typePhone("")
                .typePassword("")
                .clickTiepTuc();
        Common.sleep(2000);
    }

}
